#!/bin/bash
#########################################################
#							#
#   Change default username to a custom one		#
#							#
#   Use this script at your own risk.			#
#   I am not responsible for damage or data loss	#
#   caused by this script.				#
#							#
#   Script created by gdev, 2015			#
#   https://github.com/de-live-gdev 			#
#							#
#   Feel free to use, modify and share!			#
#							#
#########################################################

OLD="pi"
NEW="gregor"

# Information
cat <<EOF
Do not use sudo with the user to be renamed, use root login.
Set root password using
>sudo passwd<
,login as root user, and start again.

Use this script at your own risk.

EOF

# Check root access
[[ $EUID -ne 0 ]] && echo "Script not started as root" 1>&2 && exit 1

# Proceed ?
echo "Renaming user >$OLD< to >$NEW<"
read -p "Proceed? (y/n): " -n 1 -r ; echo
[[ ! $REPLY =~ ^[Yy]$ ]] && exit 1

# do stuff
usermod -m -d "/home/$NEW" -l "$NEW" "$OLD"
groupmod -n "$NEW" "$OLD"
sed -i "s/${OLD} ALL/${NEW} ALL/g" /etc/sudoers
