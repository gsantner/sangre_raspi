#!/bin/bash

#########################################################
#                                                       #
#   Basic Raspberry Pi tools for development [py/c++]	#
#                                                       #
#   Use this script at your own risk.                   #
#   I am not responsible for damage or data loss        #
#   caused by this script.                              #
#                                                       #
#   Script created by gdev, 2015                        #
#   https://github.com/de-live-gdev                     #
#                                                       #
#   Feel free to use, modify and share!                 #
#                                                       #
#########################################################


# Values
R_LOC=""
R_USR=""
EMAIL=""

# Enter some data
enterData(){
	#return	# skip when entered as constants
	#read -p "Do you want to reenter data? (y/n): " -n 1 -r ; echo
	#[[ ! $REPLY =~ ^[Yy]$ ]] && return
	read -p "Enter email: " -r EMAIL
	read -p "Enter remote address: " -r R_LOC
	read -p "Enter remote user: " -r R_USR
}

# Update system
sudo apt-get update
sudo apt-get -y upgrade

####
# Basic tools
#
sudo apt-get install -y git build-essential vim
sudo apt-get install -y cmake make gcc g++ libssl-dev curl libcurl4-openssl-dev libusb-dev libreadline-dev
sudo apt-get install -y firmware-linux firmware-linux-free firmware-linux-nonfree firmware-realtek firmware-ralink

# Python
sudo apt-get install -y python-dev python3-dev python3-pip python-pip python-setuptools python3-setuptools python-serial python3-serial
sudo apt-get install -y python-rpi.gpio python3-rpi.gpio  python-smbus python3-smbus i2c-tools
sudo apt-get install -y libxml2-dev libxslt1-dev python-lxml python3-lxml

# Bluetooth
sudo apt-get install -y python-bluez bluez bluetooth bluez-utils blueman

# Playback
sudo apt-get install -y ffmpeg sox libsox-fmt-all mpg123


# Enter data
enterData

######
# SSH
#
[[ ! -e "/home/$USER/.ssh/" ]] && mkdir "/home/$USER/.ssh/"
read -p "Load authorized keys from another device (via ssh)? (y/n): " -n 1 -r ; echo
if [[ $REPLY =~ ^[Yy]$  ]]; then
	scp "$R_USR@$R_LOC:/home/$R_USR/.ssh/authorized_keys" "/home/$USER/.ssh/authorized_keys"
fi

read -p "Generate new ssh key and allow access to remote? (y/n): " -n 1 -r ; echo
if [[ $REPLY =~ ^[Yy]$  ]]; then
	ssh-keygen -t rsa -b 4096 -C "$EMAIL"
	ssh-copy-id "$R_USR@$R_LOC"
	git config --global user.name "$USER"
	git config --global user.email "$EMAIL"
fi

# Dont load locale from local device
sudo sed -i 's/^AcceptEnv LANG LC_*/#AcceptEnv LANG LC_*/' /etc/ssh/sshd_config
sudo service ssh restart

# WiringPi
TMP=$(mktemp -d)
cd "$TMP"
git clone git://git.drogon.net/wiringPi
cd wiringPi
./build

# Python eeml
cd ..
git clone https://github.com/petervizi/python-eeml.git
cd python-eeml
sudo python setup.py install
sudo python3 setup.py install

# Python libs
sudo pip install rpi.gpio requests raspi wiringpi2 pydht2
sudo pip-3.2 install rpi.gpio requests raspi wiringpi2 pydht2

# Fetch some resources
mkdir "/home/$USER/coding"
cd "/home/$USER/coding"
git clone https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code.git
git clone https://github.com/tomhartley/AirPi.git
git clone https://github.com/Mikael-Jakhelln/PiFMPlay.git
git clone https://github.com/simonmonk/raspberrypi_cookbook.git



echo "See https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=98318 and enable needed configs."
